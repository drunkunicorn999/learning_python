def bubble_sort(arr: list, key=lambda x: x, reverse=None) -> list:  # сортировка пузырьком
    n = len(arr)

    for i in range(n-1):
        for j in range(n-i-1):
            if key(arr[j]) > key(arr[j+1]):
                arr[j], arr[j+1] = arr[j+1], arr[j]

    if reverse:
        return arr[::-1]
    else:
        return arr


def insertion_sort(arr: list, key=lambda x: x, reverse=None) -> list:  # сортировка вставками
    n = len(arr)
    for i in range(1, n):  # начинаем со второго элемента и заканчиваем последним включительно
        a = i
        while a > 0 and key(arr[a - 1]) > key(arr[a]):
            arr[a-1], arr[a] = arr[a], arr[a-1]
            a -= 1

    if reverse:
        return arr[::-1]
    else:
        return arr


def shell_sort(arr: list, key=lambda x: x, reverse=None) -> list:
    n = len(arr)
    gap = n // 2  # колличество n делим на 2 и получаем разрыв
    while gap > 0:
        for i in range(gap, n):
            if key(arr[i - gap]) > key(arr[i]):
                arr[i-gap], arr[i] = arr[i], arr[i-gap]
        gap //= 2

    if reverse:
        return arr[::-1]
    else:
        return arr


def quicksort(arr: list, key=lambda x: x, reverse=None) -> list:
    if len(arr) < 2:
        return arr
    else:
        pivot = arr[0]
        less = [i for i in arr[1:] if key(i) <= key(pivot)]
        greater = [i for i in arr[1:] if key(i) > key(pivot)]

        if reverse:
            return (quicksort(less) + [pivot] + quicksort(greater))[::-1]
        else:
            return quicksort(less) + [pivot] + quicksort(greater)


# функция для обмена элементов в списке
def swap(array, first, second):
    array[first], array[second] = array[second], array[first]


# сортировка выбором
def selection_sort(arr: list, key=lambda x: x, reverse=None) -> list:
    for i in range(len(arr) - 1):
        for k in range(i+1, len(arr)):
            if key(arr[k] < key(arr[i])):
                swap(arr, k, i)
    if reverse:
        return arr[::-1]
    else:
        return arr


def merge(left, right):
    """Берет два отсортированных списка и возвращает один отсортированный список путем сравнения
    элементов по одному.
    [1, 2, 3, 4, 5, 6]
    """
    if not left:
        return right
    if not right:
        return left
    if left[0] < right[0]:
        return [left[0]] + merge(left[1:], right)
    return [right[0]] + merge(left, right[1:])


# Tim sort
def tim_sort(the_array, reverse=False):
    runs, sorted_runs = [], []
    len_ = len(the_array)
    new_run = [the_array[0]]
    for i in range(1, len_):
        if i == len_-1:
            new_run.append(the_array[i])
            runs.append(new_run)
            break
        if the_array[i] < the_array[i-1]:
            if not new_run:
                runs.append([the_array[i-1]])
                new_run.append(the_array[i])
            else:
                runs.append(new_run)
                new_run = []
        else:
            new_run.append(the_array[i])
    for each in runs:
        sorted_runs.append(insertion_sort(each))
    sorted_array = []
    for run in sorted_runs:
        sorted_array = merge(sorted_array, run)

    if reverse:
        return sorted_array[::-1]
    else:
        return sorted_array
