from timeit import default_timer


# функция для исследования затраченного времени.
def elapsed_time(sorting, arr):
    start_time = default_timer()
    sorting(arr)
    stop_time = default_timer()
    return stop_time - start_time
