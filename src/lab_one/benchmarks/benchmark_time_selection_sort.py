import numpy as np
import matplotlib.pyplot as plt
from src.lab_one.lib.my_sorted import selection_sort
from src.lab_one.lib.functions import elapsed_time


xdata = []
ydata = []

step = 1000
min_size = 1000
max_size = 10000

for n in range(min_size, max_size + step, step):
    xdata.append(n)
    x = sorted([e for e in range(n)], reverse=True)
    ydata.append(elapsed_time(selection_sort, x))

print(xdata)
print(ydata)


xdata = np.array(xdata)
ydata = np.array(ydata)


x = np.array(xdata)
y = np.array(ydata)
z = np.polyfit(x, y, 2)
f = np.poly1d(z)

plt.style.use('seaborn')
fig, ax = plt.subplots()

ax.set_title('Сортировка выбором', fontsize=24, c='peachpuff')
ax.set_xlabel('Размер массива', fontsize=14, c='c')
ax.set_ylabel('Время выполнения', fontsize=14, c='peachpuff')

x1 = np.linspace(10_000, 50_000, 10_000)
plt.plot(x1, f(x1))
plt.plot(x, y)
plt.legend()
plt.show()
