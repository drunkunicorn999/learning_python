from src.lab_one.lib.my_sorted import bubble_sort, insertion_sort, shell_sort, quicksort, tim_sort
from random import randint
import sys

sys.setrecursionlimit(5000)

m, n = 3, 1_000
arr = [tuple(randint(-69, 69) for i in range(m)) for e in range(n)]
print(arr)


def test_check_input_values():
    try:
        sorted(arr)
    except IndexError:
        assert False
    else:
        assert True


def test_sorted():
    assert all([bubble_sort(arr) == sorted(arr),
               insertion_sort(arr) == sorted(arr),
               shell_sort(arr) == sorted(arr),
                quicksort(arr) == sorted(arr),
                tim_sort(arr) == sorted(arr)])


def test_reversed_sorted():
    assert all([(bubble_sort(arr, reverse=True) == sorted(arr, reverse=True),
                insertion_sort(arr, reverse=True) == sorted(arr, reverse=True),
                shell_sort(arr, reverse=True) == sorted(arr, reverse=True),
                 quicksort(arr, reverse=True) == sorted(arr, reverse=True),
                 tim_sort(arr, reverse=True) == sorted(arr, reverse=True))])


def test_key_sorted():
    assert all([bubble_sort(arr) == sorted(arr),
               insertion_sort(arr) == sorted(arr),
                tim_sort(arr) == sorted(arr, key=lambda x: x)])


def test_key_reversed_sorted():
    return all([bubble_sort(arr, reverse=True) == sorted(arr, key=lambda x: x, reverse=True),
                insertion_sort(arr, reverse=True) == sorted(arr, key=lambda x: x, reverse=True)])
