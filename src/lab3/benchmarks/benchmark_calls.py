import matplotlib.pyplot as plt
from src.lab3.benchmarks.ready_made_benchmarks_as_functions import callable_benchmark_calls
from src.utils.theoretical_complexity import v_e_complexity
from src.lab3.const_values import BEGIN, END, STEP


def main():
    test_size_of_array, test_average_calls_nx_complete = callable_benchmark_calls(BEGIN, END, STEP)

    fig, axes = plt.subplots(figsize=(18, 10), dpi=80)

    axes.plot(test_size_of_array, test_average_calls_nx_complete, label='Data')
    axes.plot(test_size_of_array, v_e_complexity(test_size_of_array, test_size_of_array),
              label='Theoretical complexity(Worst-case)', color='blue')

    axes.set(title='Number of operations in Graph',
             xlabel='Size of array(number of nodes)',
             ylabel='Number of operations')

    axes.grid(), axes.legend()
    plt.show()


if __name__ == "__main__":
    main()
