import cProfile
import networkx as nx
import pstats
from random import randint, choice
from src.lab3.lib.helper_functions import compare
from src.utils.useful_functions import remove_files_from_dir


class Graph:
    def __init__(self, graph_dict=None):
        """Class Graph for implementing the implementation of graph algorithms

        @param graph_dict: dict, optional. Default is None
            The input must be a dictionary. As the keys of the vertex.
            As values, a list that contains edges.
            Ex: {'E1': ['W10'],
                 'H3': ['R5', 'N2', 'Y6', 'I0'],
                 'I0': ['I0', 'H3', 'W10', 'T9', 'O8'],
                 'I4': ['N2', 'V7', 'R5'],
                 'N2': ['T9', 'V7', 'H3', 'I4'],
                 'O8': ['I0'],
                 'R5': ['H3', 'T9', 'I4', 'Y6'],
                 'T9': ['W10', 'N2', 'R5', 'T9', 'I0'],
                 'V7': ['N2', 'W10', 'I4'],
                 'W10': ['T9', 'V7', 'E1', 'I0'],
                 'Y6': ['H3', 'R5']}
            If graph_dict is None, then the attribute graph_dict = {}
        """
        if graph_dict is None:
            graph_dict = {}
        self.__graph_dict = graph_dict

    def get_graph(self):
        """Returns the current state of the graph

        @return: dict
        """
        return self.__graph_dict

    def get_nodes(self):
        """Returns the current nodes of the graph

        @return: dict
        """
        return set(self.__graph_dict.keys())

    def get_edges(self):
        """Returns the current edges of the graph

        @return: list
        """
        edges = []
        for node in self.__graph_dict:
            for neighbour in self.__graph_dict[node]:
                if {neighbour, node} not in edges:
                    edges.append({node, neighbour})
        return edges

    def get_edges_in_node(self, node):
        """Returns the edges at the given node

        @param node: var
        @return: list
        """
        if node in self.__graph_dict:
            return self.__graph_dict[node]

    def add_nodes(self, nodes):
        """Adds the specified nodes to the current graph

        @param nodes: var
        @return: None
        """
        for node in nodes:
            self.add_node(node)

    def add_node(self, node, edges=None):
        """Adds the specified node[, edges] to the current graph

        @param node: var
        @param edges: list, optional. Default is None
        @return: None
        """
        if edges is None:
            edges = []
        if node not in self.__graph_dict:
            self.__graph_dict[node] = edges

    def add_edges(self, edges):
        """Adds the specified edges to the current graph

        @param edges: list
        @return: None
        """
        for edge in edges:
            self.add_edge(edge)

    def add_edge(self, edge):
        """Adds the specified edge to the current graph. If such an
        edge is already present at the vertex, then it is not added.

        @param edge: var
        @return: None
        """
        if isinstance(edge, (tuple, list, set)) and len(edge) == 2:
            node1, node2 = edge[0], edge[1]
            if node2 not in self.__graph_dict[node1]:
                self.__graph_dict[node1].append(node2)
            if node1 not in self.__graph_dict[node2]:
                self.__graph_dict[node2].append(node1)

    def set_complete_graph_from_nx(self, number_of_nodes):
        """Sets the graph to a new state from the nx library data.
        Completely erases the current graph

        @param number_of_nodes: int
        @return: None
        """
        self.clear()
        data = nx.complete_graph(number_of_nodes)
        graph = nx.Graph(data)
        self.add_nodes(graph.nodes)
        self.add_edges(graph.edges)

    def set_fully_rare_tree_from_nx(self, number_of_nodes):
        """Sets the graph to a new state from the nx library data.
        Completely erases the current graph

        @param number_of_nodes: int
        @return: None
        """
        self.clear()
        data = nx.full_rary_tree(1, number_of_nodes)
        graph = nx.Graph(data)
        self.add_nodes(graph.nodes)
        self.add_edges(graph.edges)

    def set_random_graph(self, number_of_nodes, number_of_edges):
        """Sets a new random state based on number_of_nodes and number_of_edges
        Completely erases the current graph

        @param number_of_nodes: int
        @param number_of_edges: int
        @return: None
        """
        self.clear()
        self.__generate_random_nodes(number_of_nodes)
        self.__generate_random_edges(number_of_edges)

    def shortest_path(self, start, target):
        """BFS algorithm for finding the shortest path

        @param start: var
            Initial node
        @param target: var
            The final node
        @return: list or None
            Ex: >> shortest_path()
                ['E1', 'W10', 'T9', 'R5', 'Y6']
        """
        explored = []
        queue = [[start]]

        if start == target:
            return None

        while queue:
            path = queue.pop(0)
            node = path[-1]

            if compare(node, explored, choice=5):
                neighbours = self.__graph_dict[node]

                for neighbour in neighbours:
                    new_path = list(path)
                    new_path.append(neighbour)
                    queue.append(new_path)

                    if compare(neighbour, target):
                        return new_path

                explored.append(node)
        return None

    def clear(self):
        """Fully clears the state of the graph

        @return: None
        """
        self.__graph_dict.clear()

    def calls(self, file_name, lines_to_print):
        """A function that first profiles the method(shortest_path),
        and then analyzes and corrects the necessary data from the profiler file

        @param file_name: str
            Output file name
        @param lines_to_print: str
            Param to cProfiler function for convenient call sorting
        @return: The number of operations of the called function
        """
        nodes = list(self.__graph_dict.keys())
        file_name = f'{file_name}.txt'

        with open(file_name, 'w') as f:
            pr = cProfile.Profile()
            pr.enable()
            self.shortest_path(nodes[0], nodes[-1])
            pr.disable()
            p = pstats.Stats(pr, stream=f)
            p.strip_dirs().sort_stats().print_stats(lines_to_print)

        with open(file_name) as f:
            lines = f.readlines()
            lines = [line.strip() for line in lines]
            lines = [line for line in lines if line not in '']

        remove_files_from_dir('txt')
        return int(lines[4].split()[0])

    def __generate_random_nodes(self, value):
        """A private method that randomly generates and sets nodes
        Vertices are generated in such a way that there is one random
        letter and iteration number in the name
        Ex: >> graph.get_nodes()
            {}
            >> graph.__generate_random_nodes(5)
            >> graph.get_nodes()
            {'E1', 'H3', 'I0', 'I4', 'N2'}

        @param value: int
            Number of nodes
        @return: None
        """
        for i in range(value + 1):
            # ASCII 65 to 90 alphabet
            random_char = chr(randint(65, 90))
            self.add_node(f'{random_char}{i}')

    def __generate_random_edges(self, value):
        """A private method that randomly generates and sets edges

        @param value: int
            Number of edges
        @return: None
        """
        for i in range(0, value):
            random_edge = self.__generate_random_edge()
            self.add_edge(random_edge)

    def __generate_random_edge(self):
        """A private method that randomly generate edge
        Functions from the random - choice library are taken as a basis.

        @return: tuple
            Ex: >> graph.__generate_random_edge
                ('W10', 'I3')
        """
        nodes = tuple(self.__graph_dict.keys())
        random_edge = (choice(nodes), choice(nodes))
        return random_edge
